# Start #
This is a Tic-Tac-Toe Engine written in Java.
You can play with two players or against an artificial intelligence (AI).
The code is fully object oriented designed and easy to use and understand.
As an engine, this is the backend. You can connect any graphical or terminal-based Tic-Tac-Toe frontend you want!
It will check for valid moves, win situations and controls the AI.

# Connect a frontend #
In src/tictactoe you will find an example how to use the engine.
The frontend only has to implement the *LogicCallback* Interface.
The Interface provides you six methods for communicating with the engine:

* prepareGame

* printGame

* playerMove

* playerHasWon

* gameTied

* error

Thats all you have to do!
*prepareGame* is called when the engine wants you to prepare the frontend, if needed. As an answer, the engine waits for a *gameHasStarted*.
*printGame* is called when the engine wants you to print the gameboard. You can handle this to your frontend and print the current gameboard.
*playerMove* is called when the engine wants you to let the players move. This callback needs *playerHasMoved* as an answer so you can tell the engine what player has moved to which index.
*playerHasWon* is called when one of the player has won the game. Here you can print a "Congratulations!".
*gameTied* is called when the game has ended in a tie.
*error* informs you about errors. You can decide how to handle them properly.
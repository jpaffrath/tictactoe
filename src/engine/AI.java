package engine;

public class AI extends Logic {
	private Players aiPlayer;
	private boolean isFirstPlayer;
	private int roundCounter;

	public AI(LogicCallback callback, Players aiPlayer) {
		super(callback);
		this.aiPlayer = aiPlayer;
		this.roundCounter = 1;

		if (aiPlayer == Players.PLAYER_ONE) {
			this.isFirstPlayer = true;
		}
		else if (aiPlayer == Players.PLAYER_TWO) {
			this.isFirstPlayer = false;
		}
	}

	public void gameHasStarted() {
		if (isFirstPlayer) {
			aiMove();
		}
		else {
			callback.playerMove(Players.getOppositePlayer(aiPlayer));
		}
	}

	public void playerHasMoved(Players player, int index) {
		if (!board.setPlayerAtIndex(index, player)) {
			callback.error("Set Player '" + Players.getPlayerChar(player) + "' to " + index + " failed!");

			if (player == aiPlayer) {
				aiMove();
			}
			else {
				callback.playerMove(player);
			}
			return;
		}

		roundCounter++;

		callback.printGame(board);

		if (board.getFreeFields() == 0) {
			callback.gameTied();
			return;
		}

		if (board.checkGameboard()) {
			callback.playerHasWon(player);
			return;
		}

		if (player == aiPlayer) {
			callback.playerMove(Players.getOppositePlayer(player));
			return;
		}

		aiMove();
	}

	// this is the wrapper method for implementing several levels of difficulty
	private void aiMove() {
		playerHasMoved(aiPlayer, aiBest());
	}

	// returns the index of the first occurring critical index for the given player
	private int getCriticalIndex(Players player) {
		/*
		 * |X|X|X|
		 * |_|_|_|
		 * |_|_|_|
		 */
		if (board.getPlayerAtIndex(1) == player && board.getPlayerAtIndex(2) == player && board.getPlayerAtIndex(3) == Players.PLAYER_EMPTY) return 3;
		if (board.getPlayerAtIndex(1) == player && board.getPlayerAtIndex(3) == player && board.getPlayerAtIndex(2) == Players.PLAYER_EMPTY) return 2;
		if (board.getPlayerAtIndex(2) == player && board.getPlayerAtIndex(3) == player && board.getPlayerAtIndex(1) == Players.PLAYER_EMPTY) return 1;

		/*
		 * |_|_|_|
		 * |X|X|X|
		 * |_|_|_|
		 */
		if (board.getPlayerAtIndex(4) == player && board.getPlayerAtIndex(5) == player && board.getPlayerAtIndex(6) == Players.PLAYER_EMPTY) return 6;
		if (board.getPlayerAtIndex(4) == player && board.getPlayerAtIndex(6) == player && board.getPlayerAtIndex(5) == Players.PLAYER_EMPTY) return 5;
		if (board.getPlayerAtIndex(5) == player && board.getPlayerAtIndex(6) == player && board.getPlayerAtIndex(4) == Players.PLAYER_EMPTY) return 4;

		/*
		 * |_|_|_|
		 * |_|_|_|
		 * |X|X|X|
		 */
		if (board.getPlayerAtIndex(7) == player && board.getPlayerAtIndex(8) == player && board.getPlayerAtIndex(9) == Players.PLAYER_EMPTY) return 9;
		if (board.getPlayerAtIndex(7) == player && board.getPlayerAtIndex(9) == player && board.getPlayerAtIndex(8) == Players.PLAYER_EMPTY) return 8;
		if (board.getPlayerAtIndex(8) == player && board.getPlayerAtIndex(9) == player && board.getPlayerAtIndex(7) == Players.PLAYER_EMPTY) return 7;

		/*
		 * |X|_|_|
		 * |X|_|_|
		 * |X|_|_|
		 */
		if (board.getPlayerAtIndex(1) == player && board.getPlayerAtIndex(4) == player && board.getPlayerAtIndex(7) == Players.PLAYER_EMPTY) return 7;
		if (board.getPlayerAtIndex(1) == player && board.getPlayerAtIndex(7) == player && board.getPlayerAtIndex(4) == Players.PLAYER_EMPTY) return 4;
		if (board.getPlayerAtIndex(4) == player && board.getPlayerAtIndex(7) == player && board.getPlayerAtIndex(1) == Players.PLAYER_EMPTY) return 1;

		/*
		 * |_|X|_|
		 * |_|X|_|
		 * |_|X|_|
		 */
		if (board.getPlayerAtIndex(2) == player && board.getPlayerAtIndex(5) == player && board.getPlayerAtIndex(8) == Players.PLAYER_EMPTY) return 8;
		if (board.getPlayerAtIndex(2) == player && board.getPlayerAtIndex(8) == player && board.getPlayerAtIndex(5) == Players.PLAYER_EMPTY) return 5;
		if (board.getPlayerAtIndex(5) == player && board.getPlayerAtIndex(8) == player && board.getPlayerAtIndex(2) == Players.PLAYER_EMPTY) return 2;

		/*
		 * |_|_|X|
		 * |_|_|X|
		 * |_|_|X|
		 */
		if (board.getPlayerAtIndex(3) == player && board.getPlayerAtIndex(6) == player && board.getPlayerAtIndex(9) == Players.PLAYER_EMPTY) return 9;
		if (board.getPlayerAtIndex(3) == player && board.getPlayerAtIndex(9) == player && board.getPlayerAtIndex(6) == Players.PLAYER_EMPTY) return 6;
		if (board.getPlayerAtIndex(6) == player && board.getPlayerAtIndex(9) == player && board.getPlayerAtIndex(3) == Players.PLAYER_EMPTY) return 3;

		/*
		 * |X|_|_|
		 * |_|X|_|
		 * |_|_|X|
		 */
		if (board.getPlayerAtIndex(1) == player && board.getPlayerAtIndex(5) == player && board.getPlayerAtIndex(9) == Players.PLAYER_EMPTY) return 9;
		if (board.getPlayerAtIndex(1) == player && board.getPlayerAtIndex(9) == player && board.getPlayerAtIndex(5) == Players.PLAYER_EMPTY) return 5;
		if (board.getPlayerAtIndex(5) == player && board.getPlayerAtIndex(9) == player && board.getPlayerAtIndex(1) == Players.PLAYER_EMPTY) return 1;

		/*
		 * |_|_|X|
		 * |_|X|_|
		 * |X|_|_|
		 */
		if (board.getPlayerAtIndex(3) == player && board.getPlayerAtIndex(5) == player && board.getPlayerAtIndex(7) == Players.PLAYER_EMPTY) return 7;
		if (board.getPlayerAtIndex(3) == player && board.getPlayerAtIndex(7) == player && board.getPlayerAtIndex(5) == Players.PLAYER_EMPTY) return 5;
		if (board.getPlayerAtIndex(5) == player && board.getPlayerAtIndex(7) == player && board.getPlayerAtIndex(3) == Players.PLAYER_EMPTY) return 3;

		return -1;
	}

	// implements the best AI strategy
	private int aiBest() {
		Players enemy = Players.getOppositePlayer(aiPlayer);

		if (isFirstPlayer) {
			if (roundCounter == 1) {
				return 1;
			}
			if (roundCounter == 3) {
				if (board.getPlayerAtIndex(5) == enemy) {
					return 9;
				}
				if (board.getPlayerAtIndex(3) == enemy) {
					return 7;
				}
				if (board.getPlayerAtIndex(7) == enemy) {
					return 3;
				}
			}
		}
		else {
			if (roundCounter == 2) {
				// player starts in corner
				if (board.getPlayerAtIndex(1) == enemy ||
						board.getPlayerAtIndex(3) == enemy ||
						board.getPlayerAtIndex(7) == enemy ||
						board.getPlayerAtIndex(9) == enemy) {
					return 5;
				}
				// player starts in center
				if (board.getPlayerAtIndex(5) == enemy) {
					return 1;
				}
			}
		}

		// check if AI can win
		int critIndex = getCriticalIndex(aiPlayer);
		// if so, win the game!
		if (critIndex != -1) return critIndex;

		// check if enemy can win
		critIndex = getCriticalIndex(enemy);
		// if so, disable the field
		if (critIndex != -1) return critIndex;

		// if the AI can't find a suitable strategy, loop through the
		// fields and sets the first occurring empty field
		for (int i = 1; i < 9; i++) {
			if (board.getPlayerAtIndex(i) == Players.PLAYER_EMPTY) return i;
		}

		// if all fails, return -1 to indicate an error
		return -1;
	}
}

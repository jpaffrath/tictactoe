package engine;

public enum Players {
	PLAYER_ONE(1),
	PLAYER_TWO(6),
	PLAYER_EMPTY(0);

	private int value;

	private Players(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	// returns the corresponding player char
	public static char getPlayerChar(Players player) {
		switch (player) {
			case PLAYER_ONE: return 'X';
			case PLAYER_TWO: return 'O';
			default: break;
		}

		return ' ';
	}

	// returns the opposite player of the given player
	public static Players getOppositePlayer(Players player) {
		if (player == PLAYER_ONE) return PLAYER_TWO;
		if (player == PLAYER_TWO) return PLAYER_ONE;

		return PLAYER_EMPTY;
	}

}
package engine;

public interface LogicCallback {
	void playerMove(Players player);
	void playerHasWon(Players player);
	void gameTied();
	void prepareGame();
	void printGame(Gameboard board);
	void error(String msg);
}
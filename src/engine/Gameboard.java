package engine;

public class Gameboard {
	private Players[] board;

	public Gameboard() {
		board = new Players[9];

		// set all fields to PLAYER_EMPTY
		for (int i = 0; i < 9; i++) {
			board[i] = Players.PLAYER_EMPTY;
		}
	}

	public boolean setPlayerAtIndex(int index, Players player) {
		// the user has to enter an index between 1 and 9
		if (index < 1 || index > 9) {
			return false;
		}

		// calculates the index for the board array
		index--;

		// checks if the given index is already set with a user figure
		if (board[index] != Players.PLAYER_EMPTY) {
			return false;
		}

		// sets the given player to the index
		board[index] = player;

		return true;
	}

	public Players getPlayerAtIndex(int index) {
		// the user has to enter an index between 1 and 9
		if (index < 1 || index > 9) {
			return null;
		}

		// calculates the index for the board array
		// and returns the figure at the index
		return board[--index];
	}


	public boolean checkRows() {
		/*
		 * |X|X|X|
		 * |_|_|_|
		 * |_|_|_|
		 */
		if (board[0].getValue() + board[1].getValue() + board[2].getValue() == 3 ||
			board[0].getValue() + board[1].getValue() + board[2].getValue() == 18) {
			return true;
		}

		/*
		 * |_|_|_|
		 * |X|X|X|
		 * |_|_|_|
		 */
		if (board[3].getValue() + board[4].getValue() + board[5].getValue() == 3 ||
			board[3].getValue() + board[4].getValue() + board[5].getValue() == 18) {
			return true;
		}

		/*
		 * |_|_|_|
		 * |_|_|_|
		 * |X|X|X|
		 */
		if (board[6].getValue() + board[7].getValue() + board[8].getValue() == 3 ||
			board[6].getValue() + board[7].getValue() + board[8].getValue() == 18) {
			return true;
		}

		return false;
	}

	public boolean checkColumns() {
		/*
		 * |X|_|_|
		 * |X|_|_|
		 * |X|_|_|
		 */
		if (board[0].getValue() + board[3].getValue() + board[6].getValue() == 3 ||
			board[0].getValue() + board[3].getValue() + board[6].getValue() == 18) {
			return true;
		}

		/*
		 * |_|X|_|
		 * |_|X|_|
		 * |_|X|_|
		 */
		if (board[1].getValue() + board[4].getValue() + board[7].getValue() == 3 ||
			board[1].getValue() + board[4].getValue() + board[7].getValue() == 18) {
			return true;
		}

		/*
		 * |_|_|X|
		 * |_|_|X|
		 * |_|_|X|
		 */
		if (board[2].getValue() + board[5].getValue() + board[8].getValue() == 3 ||
			board[2].getValue() + board[5].getValue() + board[8].getValue() == 18) {
			return true;
		}

		return false;
	}

	public boolean checkDiagonals() {
		/*
		 * |X|_|_|
		 * |_|X|_|
		 * |_|_|X|
		 */
		if (board[0].getValue() + board[4].getValue() + board[8].getValue() == 3 ||
			board[0].getValue() + board[4].getValue() + board[8].getValue() == 18) {
			return true;
		}

		/*
		 * |_|_|X|
		 * |_|X|_|
		 * |X|_|_|
		 */
		if (board[2].getValue() + board[4].getValue() + board[6].getValue() == 3 ||
			board[2].getValue() + board[4].getValue() + board[6].getValue() == 18) {
			return true;
		}

		return false;
	}

	// returns true if one of the player has won the game
	public boolean checkGameboard() {
		if (checkRows())      return true;
		if (checkColumns())   return true;
		if (checkDiagonals()) return true;
		return false;
	}

	// returns the amount of free fields on the board
	public int getFreeFields() {
		int freeFields = 0;

		for (int i = 0; i < 9; i++) {
			if (board[i] == Players.PLAYER_EMPTY) {
				freeFields++;
			}
		}

		return freeFields;
	}

	// prints an easy terminal representation of the gameboard to stdout
	public void printGameboard() {
		// loop through all fields on the board
		for (int i = 0; i < 9; i++) {

			if (board[i] == Players.PLAYER_EMPTY) {
				// prints the corresponding index for an empty field
				System.out.print(i+1);
			}
			else {
				// prints the corresponding player char
				System.out.print(Players.getPlayerChar(board[i]));
			}

			// print a separator between the fields
			System.out.print("|");

			// if the current index is a value of three, perform a carriage return
			if (((i+1) % 3) == 0) {
				System.out.println();
			}
		}
	}
}
package engine;

public class Logic {
	protected Gameboard board;
	protected LogicCallback callback;
	private Players currentPlayer;

	public Logic(LogicCallback callback) {
		this.callback = callback;
		this.board = new Gameboard();
		this.currentPlayer = Players.PLAYER_ONE;
	}

	public void startGame() {
		callback.prepareGame();
	}

	public void gameHasStarted() {
		callback.playerMove(currentPlayer);
	}

	public void playerHasMoved(Players player, int index) {
		if (!board.setPlayerAtIndex(index, player)) {
			callback.error("Set Player '" + Players.getPlayerChar(player) + "' to " + index + " failed!");
			callback.playerMove(player);
			return;
		}

		callback.printGame(board);

		if (board.getFreeFields() == 0) {
			if (board.checkGameboard()) {
				callback.playerHasWon(player);
				return;
			}
			callback.gameTied();
			return;
		}

		if (board.checkGameboard()) {
			callback.playerHasWon(player);
			return;
		}

		currentPlayer = Players.getOppositePlayer(player);
		callback.playerMove(currentPlayer);
	}
}
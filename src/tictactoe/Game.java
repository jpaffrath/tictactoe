package tictactoe;

import java.util.Scanner;
import java.util.InputMismatchException;

import engine.Gameboard;
import engine.Logic;
import engine.LogicCallback;
import engine.Players;
import engine.AI;

public class Game implements LogicCallback {
	private Logic logic;
	private Scanner scanner;
	private int index;

	public Game() {
		this.scanner = new Scanner(System.in);

		// implements engine for two players
		this.logic = new Logic(this);

		// implements engine for a game against the AI
		//this.logic = new AI(this, Players.PLAYER_ONE);

		this.index = -1;
	}

	public void startGame() {
		this.logic.startGame();
	}

	@Override
	public void prepareGame() {
		System.out.println("Welcome to TicTacToe!");
		logic.gameHasStarted();
	}

	@Override
	public void playerMove(Players player) {
		while (true) {
			System.out.print("Player " + Players.getPlayerChar(player) + ", your turn: ");

			try {
				index = scanner.nextInt();
			}
			catch (InputMismatchException e) {
				// clear stdin buffer
				scanner.next();

				continue;
			}
			break;
		}

		logic.playerHasMoved(player, index);
	}

	@Override
	public void printGame(Gameboard board) {
		board.printGameboard();
		System.out.println();
	}

	@Override
	public void playerHasWon(Players player) {
		System.out.println("Player " + Players.getPlayerChar(player) + " has won the game!");
	}

	@Override
	public void gameTied() {
		System.out.println("The game ended in a tie!");
	}

	@Override
	public void error(String msg) {
		System.out.println(msg);
	}
}
